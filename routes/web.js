
const homeController =  require('../app/http/controllers/homeController')
const authController =  require('../app/http/controllers/authController')
const cartController =  require('../app/http/controllers/customer/cartController')
const guest = require('../app/http/middlewares/guest')

function initRoutes(app) {
    //home route
    app.get('/',homeController().index)

    // cart login
    app.get('/login',guest,authController().login)  // Login user not go on this page now
    app.post('/login',authController().postLogin)


    // cart register
    app.get('/register',guest,authController().register) // Login user not go on this page now
    app.post('/register',authController().postRegister)
    
    // cart logout
    app.post('/logout',authController().logout)

    // cart route  
    app.get('/cart',cartController().index)
    app.post('/update-cart',cartController().update)



    // app.get('/',(req,res)=>{
    //     res.render('home')
    // })

    // app.get('/cart',(req,res)=>{
    //     res.render('customers/cart')
    // })

    // app.get('/login',(req,res)=>{
    //     res.render('auth/login')
    // })

    // app.get('/register',(req,res)=>{
    //     res.render('auth/register')
    // })
}

module.exports = initRoutes