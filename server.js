require('dotenv').config()
const express =  require('express')
const ejs = require('ejs')
const path = require('path')
const expresslayout = require('express-ejs-layouts')
const app = express()
const PORT = process.env.PORT || 3300
const mongoose = require('mongoose')
const session = require('express-session')
const flash = require('express-flash')
const MongoDBStore = require('connect-mongo')
const passport = require('passport')

// const MongoDbStore = require('connect-mongodb-session')(session)


// Database connection [yarn add mongoose]
// const url = 'mongodb://localhost/pizza';

// mongoose.connect(url, { useNewUrlParser: true, useCreateIndex:true, useUnifiedTopology: true, useFindAndModify : true });
mongoose.connect(process.env.MONGO_CONNECTION_URL, { useNewUrlParser: true, useCreateIndex:true, useUnifiedTopology: true, useFindAndModify : true });

const connection = mongoose.connection;
connection.once('open', () => {
    console.log('Database connected...');
}).catch(err => { 
    console.log('Connection failed...')
});

//Session store
// let mongoStore = new MongoDBStore({
//     mongooseConnection: connection,
//     collection: 'sessions'
// })
 
// Session config
// install yarn add express 
// install yarn add dotenv
// install yarn add express-flash to Db session storage
// install yarn add connect-mongo  to store session inside DB
app.use(session({
    secret: process.env.COOKIE_SECRET,
    resave: false,
    store: MongoDBStore.create({
        client:connection.getClient()
        // mongoUrl:process.env.MONGO_CONNECTION_URL
    }),
    saveUninitialized: false,
    cookie: { maxAge: 1000 * 60 * 60 * 24 } // 24 hour
    // cookie: { maxAge: 1000 * 60 } // 15 sec

}));

//passport config   -- For login 
// install -> yarn add passport  passport-local
const passportInit = require('./app/config/passport')
passportInit(passport)

app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

//Assetes 
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//Global middleware
app.use((req,res,next)=> {
    res.locals.session = req.session  // To set session
    res.locals.user = req.user
    next()

})
      
// Set Template engine
app.use(expresslayout) 
app.set('views',path.join(__dirname,'/resources/views'))
app.set('view engine','ejs')

// function to pass app to web.js
require('./routes/web')(app)


app.listen(PORT,() => {
    console.log(`listening on port ${PORT}`)
})




 


// app.listen(3000,() => {
//     console.log('listening on port 3000')
// })


// if (process.env.PORT) {
//     PORT = process.env.PORT
    
// } else {
//     PORT = 3000
// }